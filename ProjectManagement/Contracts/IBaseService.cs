using System.Collections.Generic;
using ProjectManagement.Models;

namespace ProjectManagement.Contracts {
    public interface IBaseService<TEntity> where TEntity : BaseModel {
        int Add(TEntity entity);
        TEntity GetById(int id);
        IList<TEntity> GetAll();
        int Update(TEntity entity);
        int Delete(TEntity entity);
    }
}