using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Contracts;
using ProjectManagement.Models;
using System.Collections.Generic;

namespace ProjectManagement.Controllers {
    [Route("api/[controller]")]
    public class ProjectsController : Controller {
        private readonly IBaseService<Project> _projectService;
        public ProjectsController(IBaseService<Project> projectService) {
            this._projectService = projectService;
        }

        [HttpGet]
        public IEnumerable<Project> GetProjects() {
            return this._projectService.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult GetProjectById(int id) {
            if (id <= 0) {
                return NotFound();
            }
            var project = this._projectService.GetById(id);
            if (project == null) {
                return NotFound();
            }
            return Json(project);
        }
    }
}