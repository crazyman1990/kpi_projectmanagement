namespace ProjectManagement.Enums {
    public enum TaskType {
        ChangeRequest,
        NewFeature,
        Bug
    }
    public enum TaskStatus {
        Open,
        InProgress,
        Resolve,
        Complete
    }

    public enum EmployeeStatus {
        InProject,
        Idle,
        InActive
    }

    public enum Role {
        ProjectManager,
        Developer,
        Admin
    }

}