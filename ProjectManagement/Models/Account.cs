using ProjectManagement.Enums;

namespace ProjectManagement.Models {
    public class Account : Entity<int> {
        public string Username { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
    }
}