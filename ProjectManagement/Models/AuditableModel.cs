using System;

namespace ProjectManagement.Models {
    public class AuditableModel<T> : Entity<T>, IAuditableModel {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}