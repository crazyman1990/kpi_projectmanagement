namespace ProjectManagement.Models {
    public abstract class BaseModel {
        public byte[] TimeStamp { get; set; }
    }
}