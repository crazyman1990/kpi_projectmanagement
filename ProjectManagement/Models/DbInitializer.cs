using System;
using System.Collections.Generic;
using System.Linq;
using ProjectManagement.Enums;

namespace ProjectManagement.Models {

    public static class DbInitializer {
        public static void Initializer(ProjectManagementContext context) {
            context.Database.EnsureCreated();

            if (context.Projects.Any()) {
                return;
            }

            var employees = new Employee[]
            {
                new Employee() { Firstname = "Tinh", Lastname = "Nguyen", Status = EmployeeStatus.Idle },
                new Employee() { Firstname = "Thinh", Lastname = "Nguyen", Status = EmployeeStatus.Idle },
                new Employee() { Firstname = "Minh", Lastname = "Nguyen", Status = EmployeeStatus.Idle },
                new Employee() { Firstname = "Linh", Lastname = "Nguyen", Status = EmployeeStatus.Idle },
                new Employee() { Firstname = "Vinh", Lastname = "Nguyen", Status = EmployeeStatus.Idle },
            };
            foreach (Employee employee in employees) {
                context.Employees.Add(employee);
            }
            context.SaveChanges();

            var projects = new Project[]
            {
                new Project() { Name = "Project 1", Description = "Description 1", StartDate = new DateTime(2013, 2, 13)},
                new Project() { Name = "Project 2", Description = "Description 2", StartDate = new DateTime(2013, 6, 13)},
                new Project() { Name = "Project 3", Description = "Description 3", StartDate = new DateTime(2014, 8, 13)},
                new Project() { Name = "Project 4", Description = "Description 4", StartDate = new DateTime(2015, 9, 13)},
                new Project() { Name = "Project 5", Description = "Description 5", StartDate = new DateTime(2015, 11, 13)}
            };
            var index = 0;
            foreach (Project project in projects) {
                project.AssignedEmployees = new List<EmployeeAssignment>
                {
                    new EmployeeAssignment() { Project = project, Employee = employees[index++], AssignDate = new DateTime(2013, 2, 13) },
                    new EmployeeAssignment() { Project = project, Employee = employees[index++], AssignDate = new DateTime(2013, 2, 13) },
                    new EmployeeAssignment() { Project = project, Employee = employees[index++], AssignDate = new DateTime(2013, 2, 13) },
                };
                index = 0;
                context.Projects.Add(project);
            }
            context.SaveChanges();

            var sprints = new Sprint[]
            {
                new Sprint() { SprintNumber = 1, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[0] },
                new Sprint() { SprintNumber = 2, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[0] },
                new Sprint() { SprintNumber = 3, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[0] },
                new Sprint() { SprintNumber = 4, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[0] },
                new Sprint() { SprintNumber = 1, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[1] },
                new Sprint() { SprintNumber = 2, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[1] },
                new Sprint() { SprintNumber = 3, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[1] },
                new Sprint() { SprintNumber = 4, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[1] },
                new Sprint() { SprintNumber = 5, StartDate = new DateTime(2013, 2, 22), EndDate = new DateTime(2013, 3, 5), Project = projects[1] },
            };
            foreach (Sprint sprint in sprints) {
                context.Sprints.Add(sprint);
            }
            context.SaveChanges();
        }
    }

}