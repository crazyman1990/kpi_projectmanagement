using System.Collections.Generic;
using ProjectManagement.Enums;

namespace ProjectManagement.Models {
    public class Employee : Entity<int> {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int PositionId { get; set; }
        public Position Position {get; set;}
        public IList<EmployeeAssignment> Assignments { get; set; }
        public EmployeeStatus Status { get; set; }
    }
}