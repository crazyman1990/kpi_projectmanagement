using System;
using System.Collections.Generic;

namespace ProjectManagement.Models {
    public class EmployeeAssignment : BaseModel {
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime ReleaseDate { get; set; }
    }
}