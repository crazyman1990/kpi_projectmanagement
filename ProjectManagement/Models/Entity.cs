using System;

namespace ProjectManagement.Models {
    public abstract class Entity<T> : BaseModel, IEntity<T>
    {
        public T Id { get; set; }
    }
}