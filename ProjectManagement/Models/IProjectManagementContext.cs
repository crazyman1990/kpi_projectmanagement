using Microsoft.EntityFrameworkCore;

namespace ProjectManagement.Models {
    public interface IProjectManagementContext {
        //IDbSet<Project> Projects { get; set; }
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        // DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
 
        int SaveChanges();
    }
}