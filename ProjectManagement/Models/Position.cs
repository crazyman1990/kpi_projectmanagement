using System.Collections.Generic;

namespace ProjectManagement.Models {
    public class Position : Entity<int> {
        public string Name { get; set; }
        public virtual IList<Employee> Employees { get; set; }

    }
}