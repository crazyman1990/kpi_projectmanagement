using System;
using System.Collections.Generic;

namespace ProjectManagement.Models {
    public class Project : Entity<int> {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual IList<Sprint> Sprints { get; set; }
        public virtual IList<EmployeeAssignment> AssignedEmployees { get; set; }
    }
}