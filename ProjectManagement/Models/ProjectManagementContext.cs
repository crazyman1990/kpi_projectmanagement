using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using JetBrains.Annotations;

namespace ProjectManagement.Models {
    public class ProjectManagementContext : DbContext, IProjectManagementContext {
        public ProjectManagementContext(DbContextOptions<ProjectManagementContext> options) : base(options) {
            
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Sprint> Sprints { get; set; }
        public DbSet<Ticket> Tickets { get; set; }

        public override int SaveChanges() {
            var modifiedEntries = base.ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableModel && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach(var modifiedEntry in modifiedEntries) {
                var entry = modifiedEntry as IAuditableModel;
                if (entry != null) {
                    var currentDate = DateTime.Now;
                    if (modifiedEntry.State == EntityState.Added) {
                        entry.CreatedDate = currentDate;
                    } else {
                        base.Entry(entry).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entry).Property(x => x.CreatedDate).IsModified = false;
                    }
                    entry.UpdatedDate = currentDate;
                }
            }
            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder builder) 
        {
            builder.Entity<Project>().ToTable("Project").HasKey(x => x.Id);
            builder.Entity<Project>().Property(x => x.TimeStamp).IsConcurrencyToken(true);
            builder.Entity<Project>().HasMany(x => x.Sprints).WithOne(s => s.Project);

            builder.Entity<Position>().ToTable("Position").HasKey(x => x.Id);
            builder.Entity<Position>().Property(x => x.TimeStamp).IsConcurrencyToken(true);
            builder.Entity<Position>().HasMany(x => x.Employees).WithOne(e => e.Position);

            builder.Entity<Employee>().ToTable("Employee").HasKey(x => x.Id);
            builder.Entity<Employee>().Property(x => x.TimeStamp).IsConcurrencyToken(true);
            builder.Entity<Employee>().HasOne(x => x.Position).WithMany(p => p.Employees).HasForeignKey(fk => fk.PositionId);

            builder.Entity<Sprint>().ToTable("Sprint").HasKey(x => x.Id);
            builder.Entity<Sprint>().Property(x => x.TimeStamp).IsConcurrencyToken(true);
            builder.Entity<Sprint>().HasMany(x => x.Tickets).WithOne(t => t.Sprint);
            builder.Entity<Sprint>().HasOne(x => x.Project).WithMany(p => p.Sprints).HasForeignKey(fk => fk.ProjectId);

            builder.Entity<Ticket>().ToTable("Ticket").HasKey(x => x.Id);
            builder.Entity<Ticket>().Property(x => x.TimeStamp).IsConcurrencyToken(true);
            builder.Entity<Ticket>().HasOne(x => x.Sprint).WithMany(s => s.Tickets).HasForeignKey(fk => fk.SprintId);

            builder.Entity<EmployeeAssignment>().ToTable("EmployeeAssignment").HasKey(x => new { x.EmployeeId, x.ProjectId, x.AssignDate});
            builder.Entity<EmployeeAssignment>().HasOne(x => x.Employee).WithMany(e => e.Assignments).HasForeignKey(fk => fk.EmployeeId);
            builder.Entity<EmployeeAssignment>().HasOne(x => x.Project).WithMany(p => p.AssignedEmployees).HasForeignKey(fk => fk.ProjectId);
        }
    }
}