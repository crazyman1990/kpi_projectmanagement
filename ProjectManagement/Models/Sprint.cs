using System;
using System.Collections.Generic;

namespace ProjectManagement.Models {
    public class Sprint : Entity<int> {
        public int SprintNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public virtual IList<Ticket> Tickets { get; set; }
    }
}