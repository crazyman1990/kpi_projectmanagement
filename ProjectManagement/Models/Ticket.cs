using ProjectManagement.Enums;

namespace ProjectManagement.Models {
    public class Ticket : Entity<int> {
        public string Title { get; set; }
        public TaskStatus Status { get; set; }
        public TaskType TaskType { get; set; }
        public int EstimatedHours { get; set; }
        public int CompletedHours { get; set; }
        public int RemainingHours { get; set; }
        public int SprintId { get; set; }
        public Sprint Sprint { get; set; }
    }
}