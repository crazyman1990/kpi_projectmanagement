using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ProjectManagement.Contracts;
using ProjectManagement.Models;

namespace ProjectManagement.Services {
    public abstract class BaseService<TEntity> : IBaseService<TEntity> where TEntity : Entity<int>
    {
        private readonly ProjectManagementContext _context;
        protected readonly DbSet<TEntity> _dbset;
        public BaseService(ProjectManagementContext context)
        {
            this._context = context;
            this._dbset = this._context.Set<TEntity>();
        }

        public virtual int Add(TEntity entity)
        {
            this._dbset.Add(entity);
            return this._context.SaveChanges();
        }

        public virtual int Delete(TEntity entity)
        {
            if (entity == null) {
                return 1;
            }
            this._dbset.Remove(entity);
            return this._context.SaveChanges();
        }

        public virtual IList<TEntity> GetAll()
        {
            var result = this._dbset.ToListAsync();
            return result.Result;
        }

        public virtual TEntity GetById(int id)
        {
            var result = this._dbset.FirstOrDefaultAsync(x => x.Id == id);
            return result.Result;
        }

        public virtual int Update(TEntity entity)
        {
            if (entity == null) {
                return 1;
            }
            this._dbset.Update(entity);
            return this._context.SaveChanges();
        }
    }
}