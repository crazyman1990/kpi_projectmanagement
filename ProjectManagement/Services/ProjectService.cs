using Microsoft.EntityFrameworkCore;
using ProjectManagement.Models;

namespace ProjectManagement.Services {
    public class ProjectService : BaseService<Project>
    {
        public ProjectService(ProjectManagementContext context) : base(context)
        {
        }

        public override Project GetById(int id) {
            var result = this._dbset.Include(x => x.Sprints)
                            .Include(x => x.AssignedEmployees)
                            .FirstOrDefaultAsync(x => x.Id == id);
            return result.Result;
        }
    }
}