import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SelectivePreloadingStrategy } from './selective-preloading-strategy';

const appRoutes: Routes = [
    // { path: 'login', loadChildren: 'app/login/login.module#LoginModule', data: { title: 'Login'} },
    { path: '', loadChildren: 'app/home/home.module#HomeModule', data: { title: 'Homepage' } },
    // { path: '', redirectTo: '/app', pathMatch: 'full'},
    { path: '**', redirectTo: ''}
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { preloadingStrategy: SelectivePreloadingStrategy})
    ],
    exports: [
        RouterModule
    ],
    providers: [
        SelectivePreloadingStrategy
    ]
})
export class AppRoutingModule { }
