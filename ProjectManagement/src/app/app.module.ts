import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { LoginModule } from './login/login.module';
import { HomeModule } from './home/home.module';
import { ProjectModule } from './project/project.module';
import { TaskModule } from './task/task.module';

import { AppComponent } from './app.component';
// import { LoginComponent } from '../components/login/login.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        // ReactiveFormsModule,
        HttpModule,
        NgbModule.forRoot(),
        LoginModule,
        // HomeModule,
        // ProjectModule,
        // TaskModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
