import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmployeeListComponent } from './employee-list.component';

const employeeRoutes: Routes = [
    { path: '', component: EmployeeListComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(employeeRoutes)
    ]
})
export class EmployeeRoutingModule { }
