import { NgModule } from '@angular/core';
import { EmployeeRoutingModule } from './employee-routing.module';

import { EmployeeListComponent } from './employee-list.component';

@NgModule({
    imports: [
        EmployeeRoutingModule
    ],
    declarations: [
        EmployeeListComponent
    ]
})
export class EmployeeModule { }
