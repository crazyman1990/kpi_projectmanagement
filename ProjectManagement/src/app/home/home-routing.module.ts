import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';

const homeRoutes: Routes = [
    // { path: 'projects', loadChildren: 'app/project/project.module#ProjectModule' },
    // { path: 'tasks', loadChildren: 'app/task/task.module#TaskModule' },
    // { path: '', redirectTo: 'projects', pathMatch: 'full' },
    {
        path: '',
        component: HomeComponent,
        // redirectTo: '/projects',
        // pathMatch: 'full',
        children: [
            { path: '', redirectTo: 'projects', pathMatch: 'full' },
            { path: 'projects', loadChildren: 'app/project/project.module#ProjectModule' },
            { path: 'employees', loadChildren: 'app/employee/employee.module#EmployeeModule' },
            // { path: '**', redirectTo: 'projects' },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(homeRoutes)
    ],
    exports: [
        RouterModule
    ],
    // declarations: [HomeComponent]
})
export class HomeRoutingModule { }
