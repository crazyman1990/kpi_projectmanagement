import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';

@NgModule({
    imports: [
        // RouterModule,
        HomeRoutingModule
    ],
    declarations: [
        HomeComponent
    ]
})
export class HomeModule {
    // Diagnostic only: inspect router configuration
    constructor(router: Router) {
        // router.navigate(['/app/projects']);
        // console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
    }
}
