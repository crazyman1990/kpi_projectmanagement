import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.template.html'
})
export class LoginComponent implements OnInit {

    public loginForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    model = {
        username: '',
        password: ''
    };
    constructor(private _fb: FormBuilder, private _router: Router) {

    }

    ngOnInit(): void {
        this.loginForm = new FormGroup({
            username: new FormControl('', [<any>Validators.required]),
            password: new FormControl('', [<any>Validators.required])
        });
        // (<FormGroup>this.loginForm).setValue(this.model, { onlySelf: true });
    }

    onSubmit(data: any) {
        this.submitted = true;
        if (this.loginForm.valid) {
            this._router.navigate(['/']);
            console.log(this.loginForm.value);
        }
        // console.log(data);
        return false;
    }
}
