import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';

import { LoginComponent } from './login.component';

@NgModule({
    imports: [
        ReactiveFormsModule,
        LoginRoutingModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule { }
