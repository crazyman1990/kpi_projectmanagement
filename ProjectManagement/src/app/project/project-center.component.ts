import { Component } from '@angular/core';
import { Project } from '../../models/project';

@Component({
    // selector: 'app-project-center',
    template: `
    <router-outlet></router-outlet>
    `
})
export class ProjectCenterComponent { }
