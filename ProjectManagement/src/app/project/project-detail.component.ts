import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from './project.service';
import { Observable } from 'rxjs/Observable';
import { Project } from '../../models/project';

@Component({
    selector: 'app-project',
    templateUrl: './project-detail.template.html',
    // styleUrls: ['']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {

    // @Input() project: Project;
    public project: Observable<Project>;
    private id: number;
    private sub: any;

    constructor(private _projectService: ProjectService, private _activatedRoute: ActivatedRoute, private _router: Router) { 
        console.log('constructor of project detail');
    }

    ngOnInit(): void {
        // this.project = new Project({ id: 1, name: 'Project 1', startDate: new Date('2016/06/20'), endDate: null });
        console.log('init of project detail');
        this.sub = this._activatedRoute.params.subscribe(params => {
            this.id = +params['id'];
            this.project = this._projectService.getProjectById(this.id);
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    showSprints = () => this._router.navigate(['sprints'], { relativeTo: this._activatedRoute });
}
