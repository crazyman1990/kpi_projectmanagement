import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from './project.service';
import { Observable } from 'rxjs/Observable';
import { Project } from '../../models/project';

@Component({
    // selector: 'app-project-list',
    template: `
    <app-project-search [search]="searchProject" (onSearch)="search($event)"></app-project-search>
    <table class="table table-striped">
        <colgroup>
            <col style="width:10%;"/>
            <col style="width:40%;"/>
            <col style="width:25%;"/>
            <col style="width:25%;"/>
        </colgroup>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Start date</th>
            <th>End date</th>
        </tr>
        <tr *ngFor="let project of projects | async" (click)="onSelectProject(project)">
            <td>{{project.id}}</td>
            <td>{{project.name}}</td>
            <td>{{project.startDate | date:'fullDate'}}</td>
            <td>{{project.endDate | date:'fullDate'}}</td>
        </tr>
    </table>
    <button class="btn btn-default" routerLink="/projects/0">Add project</button>
    `
})
export class ProjectListComponent implements OnInit {
    public projects: Observable<Project[]>;
    public searchProject: String;

    constructor(private _projectService: ProjectService, private _activatedRoute: ActivatedRoute, private _router: Router) {
        this.searchProject = '';
    }

    ngOnInit(): void {
        this.projects = this._projectService.getProjects();
    }

    search = (data) => {
        console.log(data);
        return false;
    }

    onSelectProject = (project: Project) => {
        console.log('click on project: ', project);
        this._router.navigate([project.id], { relativeTo: this._activatedRoute });
    }
}
