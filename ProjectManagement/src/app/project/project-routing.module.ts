import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectCenterComponent } from './project-center.component';
import { ProjectListComponent } from './project-list.component';
import { ProjectDetailComponent } from './project-detail.component';

const projectRoutes: Routes = [
    {
        path: '',
        component: ProjectCenterComponent,
        children: [
            {
                path: '',
                component: ProjectListComponent
            },
            {
                path: ':id',
                component: ProjectDetailComponent,
                resolve: {

                },
                children: [
                    {
                        path: 'sprints',
                        loadChildren: 'app/sprint/sprint.module#SprintModule'
                    }
                ]
            }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(projectRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProjectRoutingModule { }
