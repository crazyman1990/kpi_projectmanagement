import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-project-search',
    template: `
    <div class="row">
        <div class="col-md-6">
            <form [formGroup]="searchForm">
                <div class="input-group">
                    <input type="text" formControlName="projectName" placeholder="Project's name" class="form-control" />
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" (click)="searchProject(searchForm.value)">Search</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    `
})
export class SearchProjectComponent implements OnInit{

    @Input() search: String;
    @Output() onSearch: EventEmitter<String>;

    public searchForm: FormGroup;

    constructor(private _fb: FormBuilder) {
        this.onSearch = new EventEmitter();
    }

    ngOnInit(): void {
        this.searchForm = this._fb.group({
            projectName: ['']
        });
    }

    searchProject = (data: any) => {
        this.onSearch.emit(data.projectName);
    }
}
