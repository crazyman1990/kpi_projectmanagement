import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ProjectRoutingModule } from './project-routing.module';

import { ProjectService } from './project.service';

import { ProjectCenterComponent } from './project-center.component';
import { ProjectListComponent } from './project-list.component';
import { ProjectDetailComponent } from './project-detail.component';
import { SearchProjectComponent } from './project-search.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ProjectRoutingModule
    ],
    declarations: [
        ProjectCenterComponent,
        ProjectDetailComponent,
        ProjectListComponent,
        SearchProjectComponent
    ],
    providers: [
        ProjectService
    ]
})
export class ProjectModule {
    constructor(router: Router) {
        // router.navigate(['/app/projects']);
        // console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
    }
}
