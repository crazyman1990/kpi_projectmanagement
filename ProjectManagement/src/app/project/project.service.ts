import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Project } from '../../models/project';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ProjectService {

    constructor(private _http: Http) {

    }

    getProjects(): Observable<Project[]> {
        return this._http.get('/api/projects').map(response => response.json() as Project[]);
    }

    getProjectById(id: number): Observable<Project> {
        console.log('get project id: ', id);
        return this._http.get('/api/projects/' + id).map(response => response.json() as Project);
    }
}
