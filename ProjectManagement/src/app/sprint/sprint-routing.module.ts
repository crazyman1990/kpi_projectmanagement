import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SprintListComponent } from './sprint-list.component';

const sprintRoutes: Routes = [
    { path: '', component: SprintListComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(sprintRoutes)
    ]
})
export class SprintRoutingModule { }
