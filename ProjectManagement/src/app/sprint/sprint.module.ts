import { NgModule } from '@angular/core';
import { SprintRoutingModule } from './sprint-routing.module';

import { SprintListComponent } from './sprint-list.component';

@NgModule({
    imports: [
        SprintRoutingModule
    ],
    declarations: [SprintListComponent]
})
export class SprintModule { }
