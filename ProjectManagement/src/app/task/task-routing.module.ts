import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TaskListComponent } from './task-list.component';

const taskRoutes: Routes = [
    { path: '', component: TaskListComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(taskRoutes)
    ]
})
export class TaskRoutingModule { }
