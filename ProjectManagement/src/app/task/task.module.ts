import { NgModule } from '@angular/core';
import { TaskRoutingModule } from './task-routing.module';

import { TaskListComponent } from './task-list.component';

@NgModule({
    imports: [
        TaskRoutingModule
    ],
    declarations: [TaskListComponent]
})
export class TaskModule { }
