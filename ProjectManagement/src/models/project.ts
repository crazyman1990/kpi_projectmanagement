export class Project {
    public id: Number;
    public name: String;
    public startDate: Date;
    public endDate: Date;

    constructor(data: any) {
        this.id = data.id;
        this.name = data.name;
        this.startDate = data.startDate;
        this.endDate = data.endDate;
    }
}
